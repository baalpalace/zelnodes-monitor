import asyncio
import logging
import sys
from datetime import datetime
from pprint import pprint as pp
from typing import List

import aiohttp
import colorama
import jsonpickle
import jstyleson
import structlog
from discord_webhook import DiscordWebhook, DiscordEmbed
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from config.config import Config
from models.discordalerts import DiscordAlerts
from models.zelnode import ZelNode_Postgresql, ZelNode_Sqlite, create_postgresql_tables, create_sqlite_tables


async def get_node_data(nodeip: str):
    """
    Retrieves zelnode data from zelflux API given IP address of node
    :param nodeip: IP address of node
    :return:
    """
    nodejson = ""
    nodeurl = f'http://{nodeip}:16127/zelflux/info'
    try:
        # Use generic catch-all header for better compatibility and less chance of DNS providers blocking the request
        headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'}
        async with aiohttp.ClientSession() as session:
            async with session.get(url=nodeurl, headers=headers) as statusresponse:
                if statusresponse.status == 200:
                    # Check for the rare case that the API doesn't return json which requires loading as a string
                    if statusresponse.content_type != 'application/json':
                        nodejson = jstyleson.loads(await statusresponse.text())
                    else:
                        nodejson = await statusresponse.json()
                await session.close()
    except:
        # Returns node IP to later report as offline
        logger.warn("Failed to reach endpoint")
        nodejson = nodeip
    return nodejson


async def get_zelcash_version() -> str:
    """
        Retrieves zelcash version from released package
        :return: Version as int
        """
    zelcashversion = "0"
    nodeurl = "https://zelcore.io/zelflux/zelcashinfo.php"
    try:
        # Use generic catch-all header for better compatibility and less chance of DNS providers blocking the request
        headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'}
        async with aiohttp.ClientSession() as session:
            async with session.get(url=nodeurl, headers=headers) as statusresponse:
                if statusresponse.status == 200:
                    # Check for the rare case that the API doesn't return json which requires loading as a string
                    if statusresponse.content_type != 'application/json':
                        versionjson = jstyleson.loads(await statusresponse.text())
                    else:
                        versionjson = await statusresponse.json()
                await session.close()
        zelcashversion = str(versionjson["version"])
    except:
        # Returns node IP to later report as offline
        logger.warn("Failed to reach endpoint")
    return zelcashversion


async def get_zelflux_version() -> str:
    """
        Retrieves zelflux version from released package
        :return: Version as str
        """
    zelfluxversion = ""
    nodeurl = "https://raw.githubusercontent.com/zelcash/zelflux/master/package.json"
    try:
        # Use generic catch-all header for better compatibility and less chance of DNS providers blocking the request
        headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'}
        async with aiohttp.ClientSession() as session:
            async with session.get(url=nodeurl, headers=headers) as statusresponse:
                if statusresponse.status == 200:
                    # Check for the rare case that the API doesn't return json which requires loading as a string
                    if statusresponse.content_type != 'application/json':
                        versionjson = jstyleson.loads(await statusresponse.text())
                    else:
                        versionjson = await statusresponse.json()
                await session.close()
        zelfluxversion = versionjson["version"]
    except:
        # Returns node IP to later report as offline
        logger.warn("Failed to reach endpoint")
    return zelfluxversion


async def get_zelbench_version() -> str:
    """
        Retrieves zelbench version from released package
        :return: Version as str
        """
    zelbenchversion = ""
    nodeurl = "https://zelcore.io/zelflux/zelbenchinfo.php"
    try:
        # Use generic catch-all header for better compatibility and less chance of DNS providers blocking the request
        headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'}
        async with aiohttp.ClientSession() as session:
            async with session.get(url=nodeurl, headers=headers) as statusresponse:
                if statusresponse.status == 200:
                    # Check for the rare case that the API doesn't return json which requires loading as a string
                    if statusresponse.content_type != 'application/json':
                        versionjson = jstyleson.loads(await statusresponse.text())
                    else:
                        versionjson = await statusresponse.json()
                await session.close()
        zelbenchversion = versionjson["version"]
    except:
        # Returns node IP to later report as offline
        logger.warn("Failed to reach endpoint")
    return zelbenchversion


async def populate_node_data(nodeslist: list) -> list:
    """
    Process data from each node into a list
    :param nodeslist: List of data for each node
    :return: List of node data
    """
    nodedatalist = []
    for node in nodeslist:
        nodedata = await get_node_data(node)
        if nodedata != "":
            nodedatalist.append(nodedata)
    return nodedatalist


async def parse_node_status(nodedatalist, config: Config) -> list:
    """
    Parses the status of each node based on retrieved data.
    If no data was returned, it is considered as Offline.
    Send optional Discord notification, if enabled, via webhook.
    :param nodedatalist: List of retrieved node data, if available
    :param config: Config instance to retrieve Discord settings
    :return: List of validated nodes.  Offline nodes will not be returns to prevent saving to DB
    """

    # Create list of valid node data to return (does not return Offline nodes)
    checkednodedatalist = []

    # Create list to hold all nodes to report to Discord
    discordalertslist = []

    # Create data container to hold Discord alert properties to store in discordalertlist
    discordalertdata = {}
    for nodedata in nodedatalist:
        try:
            # Json nodes to check
            nodeip = nodedata["data"]["zelflux"]["ip"]
            nodestatus = str(nodedata["data"]["zelnode"]["status"]["status"]).upper()

            # Nodes reporting "Expired" or "Starting" will not have a tier status
            if nodestatus.lower() != "confirmed":
                nodetier = "N/A"
            else:
                nodetier = str(nodedata["data"]["zelnode"]["status"]["tier"]).upper()
            nodebenchstatus = str(nodedata["data"]["zelbench"]["status"]["benchmarking"]).upper()
            zelcashversion = str(nodedata["data"]["zelcash"]["info"]["version"])
            zelfluxversion = str(nodedata["data"]["zelflux"]["version"])
            zelbenchversion = str(nodedata["data"]["zelbench"]["info"]["version"])

            # Retrieve official release versions for zelcash, zelflux, and zelbench
            releasezelcashversion = await get_zelcash_version()
            releasezelfluxversion = await get_zelflux_version()
            releasezelbenchversion = await get_zelbench_version()

            # Create new DiscordAlert class if node is not offline - otherwise it will be an exception
            da = DiscordAlerts(nodeip=nodeip,
                               nodetier=nodetier,
                               nodestatus=nodestatus,
                               benchstatus=nodebenchstatus,
                               zelcashversion=zelcashversion,
                               zelfluxversion=zelfluxversion,
                               zelbenchversion=zelbenchversion,
                               releasedzelcashversion=releasezelcashversion,
                               releasedzelfluxversion=releasezelfluxversion,
                               releasedzelbenchversion=releasezelbenchversion)
            if config.discordenabled is True:
                discordalertslist.append(da)
            logging.info(pp(jsonpickle.encode(da)))
            checkednodedatalist.append(nodedata)
        except:
            logger.warn("Failed to read node status")
            if config.discordenabled is True:
                if type(nodedata) is str:
                    da = DiscordAlerts(nodeip=nodedata,
                                       nodetier="N/A",
                                       nodestatus="OFFLINE",
                                       benchstatus="N/A",
                                       zelcashversion="N/A",
                                       zelfluxversion="N/A",
                                       zelbenchversion="N/A")
                    if config.discordenabled is True:
                        discordalertslist.append(da)
                    logger.warn(pp(jsonpickle.encode(da)))
    if config.discordenabled is True:
        await send_discord_alert(discordalertslist, config)
    return checkednodedatalist


async def send_discord_alert(discordalertlist: List[DiscordAlerts], config: Config):
    """
    Sends Discord notification
    :param discordalertlist: List of nodes to report checked results
    :param config: Config instance to retrieve Discord settings
    :return: None
    """
    webhook = DiscordWebhook(config.discordwebhook)

    # Do not include mention on a fully healthy report
    allconfirmed = False
    for node in discordalertlist:
        # If all nodes report as Confirmed and all versions equal release versions, there is no need to report any issue
        if node.nodestatus.lower() == "confirmed" and DiscordAlerts.version_compare(node.zelcashversion, node.releasedzelcashversion) == 0 and DiscordAlerts.version_compare(node.zelfluxversion, node.releasedzelfluxversion) == 0 and DiscordAlerts.version_compare(node.zelbenchversion, node.releasedzelbenchversion) == 0:
            allconfirmed = True
            node.zelcashversion += " (Up-to-date)"
            node.zelfluxversion += " (Up-to-date)"
            node.zelbenchversion += " (Up-to-date)"
        else:
            # Break loop if any value other than Confirmed is found or any version does not equal release version after setting allconfirmed to False
            allconfirmed = False
            break
    if allconfirmed is False:
        # Mentions/pings must be placed in webhook content instead of embed
        # Always use the below format for a mention <@{discord_user_id}> (not string name)
        webhook.content = f"Attention <@{config.nodeadminid}>"
        embedtitle = "Zelnode Issues Found!"

        # Loop through nodes to set any version issues (0 = same, 1 = can update, -1 = node is somehow newer than release)
        for node in discordalertlist:
            if DiscordAlerts.version_compare(node.zelcashversion, node.releasedzelcashversion) == 0:
                node.zelcashversion += " (Update-to-date)"
            elif DiscordAlerts.version_compare(node.zelcashversion, node.releasedzelcashversion) == 1:
                node.zelcashversion += f" (Update to {node.releasedzelcashversion} available)"
            elif DiscordAlerts.version_compare(node.zelcashversion, node.releasedzelcashversion) == -1:
                node.zelcashversion += f" (Version is newer than release: {node.releasedzelcashversion})"

            if DiscordAlerts.version_compare(node.zelfluxversion, node.releasedzelfluxversion) == 0:
                node.zelfluxversion += " (Update-to-date)"
            elif DiscordAlerts.version_compare(node.zelfluxversion, node.releasedzelfluxversion) == 1:
                node.zelfluxversion += f" (Update to {node.releasedzelfluxversion} available)"
            elif DiscordAlerts.version_compare(node.zelfluxversion, node.releasedzelfluxversion) == -1:
                node.zelfluxversion += f" (Version is newer than release: {node.releasedzelfluxversion})"

            if DiscordAlerts.version_compare(node.zelbenchversion, node.releasedzelbenchversion) == 0:
                node.zelbenchversion += " (Update-to-date)"
            elif DiscordAlerts.version_compare(node.zelbenchversion, node.releasedzelbenchversion) == 1:
                node.zelbenchversion += f" (Update to {node.releasedzelbenchversion} available)"
            elif DiscordAlerts.version_compare(node.zelbenchversion, node.releasedzelbenchversion) == -1:
                node.zelbenchversion += f" (Version is newer than release: {node.releasedzelbenchversion})"
    else:
        embedtitle = "Zelnode Health Good!"

    embed = DiscordEmbed(title=embedtitle)

    count = 1
    # TODO Refactor in the future to handle more nodes per embed as field limit is 25/embed
    for node in discordalertlist:
        # Use Node IP as sub-heading so embed field must be inline=False while the remaining are inline=True
        embed.add_embed_field(name="Node Info", value=f"Node IP: {node.nodeip}\nNode Tier: {node.nodetier}", inline=False)
        embed.add_embed_field(name="Statuses", value=f"Node Status: {node.nodestatus}\nBench Status: {node.benchstatus}", inline=False)
        embed.add_embed_field(name="Versions", value=f"Zelcash: {node.zelcashversion}\nZelflux: {node.zelfluxversion}\nZelbench: {node.zelbenchversion}", inline=False)
        if count < len(discordalertlist):
            embed.add_embed_field(name="\u200b", value="\u200b", inline=False)
    webhook.add_embed(embed)
    webhook.execute()


async def main():
    # Read config file into Config() class
    config = Config()
    config.read_config()

    # Transfer node IP's to list
    nodeslist = config.nodelist

    # Retrieve all node data
    nodedatalist = await populate_node_data(nodeslist)

    # Create list to hold DB entities
    entitylist = []

    # Proceed if at least one or more entities were populated
    if len(nodedatalist) > 0:

        # Create timestamp instance so that the group of DB inserts will all have the same insert timestamp (for better data retrieval grouping)
        timestampinstance = datetime.utcnow()

        if config.databasetype == "postgresql":
            engine = create_engine(f"postgresql://{config.dbusername}:{config.dbpassword}@{config.dbaddress}/{config.db}")
            Session = sessionmaker(bind=engine)
            session = Session()

            # Send node list to check status and send Discord alerts if enabled
            checkednodelist = await parse_node_status(nodedatalist, config)
            for node in checkednodelist:
                zelnode = ZelNode_Postgresql()
                zelnode.nodedata = node
                zelnode.timestamp = timestampinstance
                entitylist.append(zelnode)
                if len(entitylist) > 0:
                    create_postgresql_tables(engine)
                    ZelNode_Postgresql.save_to_database(entitylist, session)
        elif config.databasetype == "sqlite":
            engine = create_engine(f"sqlite:///{config.dbfile}")
            Session = sessionmaker(bind=engine)
            session = Session()

            # Send node list to check status and send Discord alerts if enabled
            checkednodelist = await parse_node_status(nodedatalist, config)
            for node in checkednodelist:
                zelnode = ZelNode_Sqlite()
                zelnode.nodedata = node
                zelnode.timestamp = timestampinstance
                entitylist.append(zelnode)
                if len(entitylist) > 0:
                    create_sqlite_tables(engine)
                    ZelNode_Sqlite.save_to_database(entitylist, session)


if __name__ == '__main__':
    # Logging setup
    logging.basicConfig(
        stream=sys.stdout, level=logging.INFO
    )
    structlog.configure(
        processors=[
            structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M:%S", utc=True),
            structlog.processors.format_exc_info,
            structlog.processors.KeyValueRenderer(
                key_order=["timestamp", "event"],
                drop_missing=True
            ),
        ],
        context_class=structlog.threadlocal.wrap_dict(dict),
        logger_factory=structlog.stdlib.LoggerFactory(),
        wrapper_class=structlog.stdlib.BoundLogger,
    )
    logger = structlog.get_logger(__name__)
    colorama.init(convert=True)
    # Logging setup end

    # Create async loop to run main as async
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    except KeyboardInterrupt:
        pass
    except:
        logger.exception("Something went wrong")
    finally:
        loop.close()
